**# Revature P1 Reimbursement**
---------------------------------------------------
**Project Description**

    A web application where Employees can post requests for reimbursement payments and Managers can approve or deny the requests. Postgres Database will be used and meant to be deployed on AWS RDS.
    Currently, the project is not in a fully functional state. In the application.properties file, where the database URL says jdbc:postgresql: //postgres. cuiwdhamwtzc.us-east-2. rds.amazonaws.com:5432/postgres , that would have to be changed to a different database url (whether as a localhost server or another RDS server) because the rds server the url corresponds to had been taken down. Note: Spaces were added in the url in the readme because it was being mistaken for a link that doesn't work.

--------------------------------------------------
**Author's note:**
<details><summary>Click to expand</summary>
    As a solo project, this proved to be a lot harder than I thought it was going to be at first. I still have much to learn about Javalin and how to successfully connect the front end and back end of the application so that way the end result is functional while using a Graphical User Interface Web Browser as opposed to using a Command Line Interface.
</details>

--------------------------------------------------
**Technologies Used**

    - Java 1.8
    - HTML
    - Amazon RDS
    - Postgresql
    - Javalin / Spring dependencies (The main branch tries to use Javalin because the project requires it, but another branch attempts to use Spring dependencies to try and make the project functional)

----------------------------------------------------
**Features**

    - Use endpoints to view the tables in the database

------------------------------------------------------
**To-Do List**

    - Create HTML pages that connect the front end and back end.
    - Incorporate Javalin instead of Spring as the requirements state
    - Test the program to see that it is functional
