/**
 * DAO class for the users table
 */
package com.revature.roborevaturep1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.revature.roborevaturep1.model.UsersEntity;

/**
 * @author Dipanjali Ghosh
 *
 */
public class UsersDAO {

	private static Connection conn = null;
	private static Statement stmt = null;
	private static PreparedStatement pstmt = null;
	private static ResultSet rs = null;

	public static List<UsersEntity> readAll() {
		List<UsersEntity> users = new ArrayList<UsersEntity>();
		String query = "select * from public.users";
		connect();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				UsersEntity myUser = new UsersEntity();
				myUser.setUser_id(rs.getInt("user_id"));
				myUser.setUsername(rs.getString("username"));
				myUser.setPassword(rs.getString("password"));
				myUser.setRole_number(rs.getInt("role_number"));
				myUser.setReal_name(rs.getString("real_name"));
				myUser.setEmail(rs.getString("email"));
				myUser.setMobile(rs.getLong("mobile"));
				users.add(myUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return users;
	}

	public static UsersEntity readById(int id) {
		UsersEntity myUser = new UsersEntity();
		String query = "select * from public.users where user_id=" + id;
		connect();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
				myUser.setUser_id(rs.getInt("user_id"));
				myUser.setUsername(rs.getString("username"));
				myUser.setPassword(rs.getString("password"));
				myUser.setRole_number(rs.getInt("role_number"));
				myUser.setReal_name(rs.getString("real_name"));
				myUser.setEmail(rs.getString("email"));
				myUser.setMobile(rs.getLong("mobile"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return myUser;
	}
	
	public static UsersEntity logMeIn(String username, String password) {
		UsersEntity myUser = new UsersEntity();
		String query = "select * from public.users where username='" + username
				+ "' and password='" + password +"';";
		connect();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
				myUser.setUser_id(rs.getInt("user_id"));
				myUser.setUsername(rs.getString("username"));
				myUser.setPassword(rs.getString("password"));
				myUser.setRole_number(rs.getInt("role_number"));
				myUser.setReal_name(rs.getString("real_name"));
				myUser.setEmail(rs.getString("email"));
				myUser.setMobile(rs.getLong("mobile"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return myUser;
	}

	public static void save(UsersEntity newUser) {
		String query = "insert into public.users (username, password, real_name, email, mobile, role_number) values (?, ?, ?, ?, ?, ?);";
		connect();

		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, newUser.getUsername());
			pstmt.setString(2, newUser.getPassword());
			pstmt.setString(3, newUser.getReal_name());
			pstmt.setString(4, newUser.getEmail());
			pstmt.setLong(5, newUser.getMobile());
			pstmt.setInt(6, newUser.getRole_number());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}

	public static void delete(int id) {
		String query = "delete from public.users where user_id = " + id + ";";
		connect();
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}

	public static void update(int id, UsersEntity newUser) {
		String query = "update public.users set username=?, password=?, real_name=?, email=?, mobile=?, role_number=? where user_id=?;";
		connect();
		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, newUser.getUsername());
			pstmt.setString(2, newUser.getPassword());
			pstmt.setString(3, newUser.getReal_name());
			pstmt.setString(4, newUser.getEmail());
			pstmt.setLong(5, newUser.getMobile());
			pstmt.setInt(6, newUser.getRole_number());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}

	public static Connection connect() {
		String dbUrl = "jdbc:postgresql://postgres.cuiwdhamwtzc.us-east-2.rds.amazonaws.com:5432/postgres";
		try {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(dbUrl, "postgres", "postgres");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void close() {
		try {
			if (conn != null) {
				conn.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (pstmt != null) {
				pstmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
