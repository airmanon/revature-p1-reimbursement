/**
 * Entity class for the users table
 */
package com.revature.roborevaturep1.model;

/**
 * @author Dipanjali Ghosh
 *
 */
public class UsersEntity {
	private int user_id;
	private String username;
	private String password;
	private String real_name;
	private String email;
	private long mobile;
	private int role_number;
	
	public UsersEntity() {
		super();
	}

	public UsersEntity(int user_id, String username, String password, String real_name, String email, long mobile,
			int role_number) {
		super();
		this.user_id = user_id;
		this.username = username;
		this.password = password;
		this.real_name = real_name;
		this.email = email;
		this.mobile = mobile;
		this.role_number = role_number;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getReal_name() {
		return real_name;
	}

	public void setReal_name(String real_name) {
		this.real_name = real_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	public int getRole_number() {
		return role_number;
	}

	public void setRole_number(int role_number) {
		this.role_number = role_number;
	}

	@Override
	public String toString() {
		return "UsersEntity [user_id=" + user_id + ", username=" + username + ", password=" + password + ", real_name="
				+ real_name + ", email=" + email + ", mobile=" + mobile + ", role_number=" + role_number + "]";
	}
}
