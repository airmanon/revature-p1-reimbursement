/**
 * DAO Class for the staff_roles table
 */
package com.revature.roborevaturep1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.revature.roborevaturep1.model.StaffRoles;

/**
 * @author Dipanjali Ghosh
 *
 */
public class StaffRolesDAO {

	private static Connection conn = null;
	private static Statement stmt = null;
	private static PreparedStatement pstmt = null;
	private static ResultSet rs = null;

	public static List<StaffRoles> readAll() {
		List<StaffRoles> roles = new ArrayList<StaffRoles>();
		String query = "select * from public.staff_roles";
		connect();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				StaffRoles myRole = new StaffRoles();
				myRole.setRole_number(rs.getInt("role_number"));
				myRole.setRole_name(rs.getString("role_name"));
				roles.add(myRole);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return roles;
	}

	public static StaffRoles readById(int id) {
		StaffRoles myRole = new StaffRoles();
		String query = "select * from public.staff_roles where role_number=" + id;
		connect();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {

				myRole.setRole_number(rs.getInt("role_number"));
				myRole.setRole_name(rs.getString("role_name"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return myRole;
	}

	public static void save(StaffRoles role) {
		String query = "insert into public.staff_roles (role_name) values (?);";
		connect();

		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, role.getRole_name());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}

	public static void delete(int id) {
		String query = "delete from public.staff_roles where role_number = " + id + ";";
		connect();
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}

	public static void update(int id, StaffRoles role) {
		String query = "update public.staff_roles set role_name=? where role_number=?;";
		connect();
		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, role.getRole_name());
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}

	public static Connection connect() {
		String dbUrl = "jdbc:postgresql://postgres.cuiwdhamwtzc.us-east-2.rds.amazonaws.com:5432/postgres";
		try {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(dbUrl, "postgres", "postgres");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void close() {
		try {
			if (conn != null) {
				conn.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (pstmt != null) {
				pstmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}