/**
 * Entity class for the staff_roles table in the Database
 */
package com.revature.roborevaturep1.model;

/**
 * @author Dipanjali Ghosh
 *
 */
public class StaffRoles {
	private int role_number;
	private String role_name;
	
	public StaffRoles() {
		super();
	}

	public StaffRoles(int role_number, String role_name) {
		super();
		this.role_number = role_number;
		this.role_name = role_name;
	}

	public int getRole_number() {
		return role_number;
	}

	public void setRole_number(int role_number) {
		this.role_number = role_number;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	@Override
	public String toString() {
		return "StaffRoles [role_number=" + role_number + ", role_name=" + role_name + "]";
	}
}
