/**
 * Starter method for this project.
 */
package com.revature.roborevaturep1;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.revature.roborevaturep1.dao.RequestDAO;
import com.revature.roborevaturep1.dao.StaffRolesDAO;
import com.revature.roborevaturep1.dao.UsersDAO;
import com.revature.roborevaturep1.model.RequestEntity;
import com.revature.roborevaturep1.model.StaffRoles;
import com.revature.roborevaturep1.model.UsersEntity;
import com.revature.roborevaturep1.util.LogMeOut;
import com.revature.roborevaturep1.util.MyLogin;

import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;

/**
 * @author Dipanjali Ghosh
 *
 */
public class App {
	private static MyLogin currentUser = new MyLogin(new StaffRoles(), new UsersEntity());

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/public", Location.CLASSPATH);
		}).start(7000);

		Logger logger = LoggerFactory.getLogger(App.class);

		app.get("/test", ctx -> ctx.html("<h1>Welcome to Javalin </h1>"));

		app.get("/", ctx -> ctx.redirect("index.html"));

		List<StaffRoles> roles = StaffRolesDAO.readAll();

		app.get("/roles", ctx -> ctx.jsonStream(roles));

		List<UsersEntity> allUsers = UsersDAO.readAll();

		app.get("/users", ctx -> ctx.jsonStream(allUsers));

		List<RequestEntity> allRequests = RequestDAO.readAll();

		app.get("/requests", ctx -> ctx.jsonStream(allRequests));

		app.get("/roles/{id}", ctx -> {
			int id = Integer.parseInt(ctx.pathParam("id"));
			StaffRoles role = StaffRolesDAO.readById(id);
			ctx.json(role);
		});

		// very simple error page just in case
		app.get("/error", ctx -> ctx.html("<h1>Sorry! Error Happened!</h1>"));

		app.post("/loginForm", ctx -> {
			try {
				currentUser.setMyUser(UsersDAO.logMeIn(ctx.formParam("username"), ctx.formParam("password")));
				currentUser.setMyRole(StaffRolesDAO.readById(currentUser.getMyUser().getRole_number()));
				logger.info("This is the currentUser object: " + currentUser);
				if (currentUser.getMyRole().getRole_name().equalsIgnoreCase("Employee")) {
					//ctx.notifyAll();
					ctx.redirect("employeeMenu.html", 200);
				} else if (currentUser.getMyRole().getRole_name().equalsIgnoreCase("Manager")) {
					//ctx.notifyAll();
					ctx.redirect("managerMenu.html", 200);
				} else {
					ctx.html("<h1>Invalid login. Please try again</h1>");
					//ctx.wait(60000); // should wait a minute before continuing
					ctx.redirect("index.html");
				}
			} catch (NullPointerException e) {
				ctx.html("<h1>Null Pointer Exception. currentUser is null!</h1>");
				//ctx.wait(10000); // should wait 10 seconds.
				ctx.redirect("index.html");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		app.get("/logout", ctx -> {
			currentUser = LogMeOut.logout();
			ctx.redirect("index.html");
		});
		
		app.post("roles", ctx -> {
			StaffRoles role = ctx.bodyAsClass(StaffRoles.class);
			logger.info("This the StaffRoles object from postman :" + role);
			StaffRolesDAO.save(role);
			ctx.status(201);
			ctx.redirect("students.html");
		});
		
		app.delete("roles/{id}", ctx -> {
			int id = Integer.parseInt(ctx.pathParam("id"));
			StaffRolesDAO.delete(id);
			ctx.status(200);
			ctx.html("<h1>Role Deleted</h1>");
		});
		
		app.put("roles/{id}", ctx -> {
		int id = Integer.parseInt(ctx.pathParam("id"));
		StaffRoles role = ctx.bodyAsClass(StaffRoles.class);
		StaffRolesDAO.update(id, role);
		ctx.status(200);
		ctx.html("<h1>Role Updated</h1>");
		});

//		app.get("studentForm", ctx -> ctx.redirect("addStudent.html"));
//
//		app.post("studentsForm", ctx -> {
//
//			String name = ctx.formParam("name");
//			String email = ctx.formParam("email");
//			Long mobile = Long.parseLong(ctx.formParam("mobile"));
//			int courseId = Integer.parseInt(ctx.formParam("courseId"));
//			Student stu = new Student(name, email, mobile, courseId);
//			logger.info("This the student object from postman :" + stu);
//			StudentDAO.save(stu);
//			ctx.status(201);
//			ctx.redirect("students.html");
//		});

//		app.put("students/{id}", ctx -> {
//			int id = Integer.parseInt(ctx.pathParam("id"));
//			Student stu = ctx.bodyAsClass(Student.class);
//			StudentDAO.update(id, stu);
//			ctx.status(200);
//			ctx.redirect("students.html");
//		});

//		app.delete("students/{id}", ctx -> {
//			int id = Integer.parseInt(ctx.pathParam("id"));
//			StudentDAO.delete(id);
//			ctx.status(200);
//			ctx.redirect("students.html");
//		});

//		List<Course> courses = CourseDAO.findAll();
//
//		app.get("/course", ctx -> ctx.jsonStream(courses));
//
//		app.get("/course/{id}", ctx -> {
//			int id = Integer.parseInt(ctx.pathParam("id"));
//			Course course = CourseDAO.findById(id);
//			ctx.json(course);
//		});
//
//		app.post("course", ctx -> {
//			Course cour = ctx.bodyAsClass(Course.class);
////			logger.info("This the student object from postman :" + stu);
//			CourseDAO.save(cour);
//			ctx.status(201);
//			ctx.redirect("courses.html");
//		});
//		
//		app.get("courseForm", ctx -> ctx.redirect("addCourse.html"));
//
//		app.post("courseForm", ctx -> {
//
//			String name = ctx.formParam("name");
//			String category = ctx.formParam("category");
//			
//			int duration = Integer.parseInt(ctx.formParam("duration"));
//			Course course = new Course(name, category, duration);
//			logger.info("This the Course object from postman :" + course);
//			CourseDAO.save(course);
//			ctx.status(201);
//			ctx.redirect("courses.html");
//		});
//
//		app.put("course/{id}", ctx -> {
//			int id = Integer.parseInt(ctx.pathParam("id"));
//			Course cour = ctx.bodyAsClass(Course.class);
//			CourseDAO.update(id, cour);
//			ctx.status(200);
//			ctx.redirect("courses.html");
//		});
//
//		app.delete("course/{id}", ctx -> {
//			int id = Integer.parseInt(ctx.pathParam("id"));
//			CourseDAO.delete(id);
//			ctx.status(200);
//			ctx.redirect("courses.html");
//		});

	}

}
