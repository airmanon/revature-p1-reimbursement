/**
 * This file is just for the purpose of ensuring that when the user logs out, the MyLogin object is effectively cleared.
 */
package com.revature.roborevaturep1.util;

import com.revature.roborevaturep1.model.StaffRoles;
import com.revature.roborevaturep1.model.UsersEntity;

/**
 * @author Dipanjali Ghosh
 *
 */
public class LogMeOut {
	public static MyLogin logout() {
		return new MyLogin(new StaffRoles(), new UsersEntity());
	}
}
