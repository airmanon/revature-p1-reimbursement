/**
 * Entity class for the request table
 */
package com.revature.roborevaturep1.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author Dipanjali Ghosh
 *
 */
public class RequestEntity {
	private int request_id;
	private int employee_id;
	private BigDecimal amount;
	private String employee_reason;
	private Timestamp time_requested;
	private int manager_id;
	private boolean is_approved;
	private String manager_reason;
	private Timestamp time_reviewed;
	
	public RequestEntity() {
		super();
	}
	
	//Constructor for when an employee makes a new request
	public RequestEntity(int employee_id, BigDecimal amount, String employee_reason, Timestamp time_requested) {
		super();
		this.employee_id = employee_id;
		this.amount = amount;
		this.employee_reason = employee_reason;
		this.time_requested = time_requested;
	}

	public RequestEntity(int request_id, int employee_id, BigDecimal amount, String employee_reason,
			Timestamp time_requested, int manager_id, boolean is_approved, String manager_reason,
			Timestamp time_reviewed) {
		super();
		this.request_id = request_id;
		this.employee_id = employee_id;
		this.amount = amount;
		this.employee_reason = employee_reason;
		this.time_requested = time_requested;
		this.manager_id = manager_id;
		this.is_approved = is_approved;
		this.manager_reason = manager_reason;
		this.time_reviewed = time_reviewed;
	}

	public int getRequest_id() {
		return request_id;
	}

	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}

	public int getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getEmployee_reason() {
		return employee_reason;
	}

	public void setEmployee_reason(String employee_reason) {
		this.employee_reason = employee_reason;
	}

	public Timestamp getTime_requested() {
		return time_requested;
	}

	public void setTime_requested(Timestamp time_requested) {
		this.time_requested = time_requested;
	}

	public int getManager_id() {
		return manager_id;
	}

	public void setManager_id(int manager_id) {
		this.manager_id = manager_id;
	}

	public boolean isIs_approved() {
		return is_approved;
	}

	public void setIs_approved(boolean is_approved) {
		this.is_approved = is_approved;
	}

	public String getManager_reason() {
		return manager_reason;
	}

	public void setManager_reason(String manager_reason) {
		this.manager_reason = manager_reason;
	}

	public Timestamp getTime_reviewed() {
		return time_reviewed;
	}

	public void setTime_reviewed(Timestamp time_reviewed) {
		this.time_reviewed = time_reviewed;
	}

	@Override
	public String toString() {
		return "RequestEntity [request_id=" + request_id + ", employee_id=" + employee_id + ", amount=" + amount
				+ ", employee_reason=" + employee_reason + ", time_requested=" + time_requested + ", manager_id="
				+ manager_id + ", is_approved=" + is_approved + ", manager_reason=" + manager_reason
				+ ", time_reviewed=" + time_reviewed + "]";
	}
}
