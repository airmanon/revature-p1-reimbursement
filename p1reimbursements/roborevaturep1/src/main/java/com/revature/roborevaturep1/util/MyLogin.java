/**
 * This class is used just to help keep track of the user currently logged in.
 */
package com.revature.roborevaturep1.util;

import com.revature.roborevaturep1.model.StaffRoles;
import com.revature.roborevaturep1.model.UsersEntity;

/**
 * @author Dipanjali Ghosh
 *
 */
public class MyLogin {
	private StaffRoles myRole;
	private UsersEntity myUser;
	
	public MyLogin() {
		super();
	}
	
	public MyLogin(StaffRoles myRole, UsersEntity myUser) {
		super();
		this.myRole = myRole;
		this.myUser = myUser;
	}
	
	public StaffRoles getMyRole() {
		return myRole;
	}
	
	public void setMyRole(StaffRoles myRole) {
		this.myRole = myRole;
	}
	
	public UsersEntity getMyUser() {
		return myUser;
	}
	
	public void setMyUser(UsersEntity myUser) {
		this.myUser = myUser;
	}
	
	@Override
	public String toString() {
		return "MyLogin [myRole=" + myRole + ", myUser=" + myUser + "]";
	}
}
