/**
 * DAO class for the request table
 */
package com.revature.roborevaturep1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.revature.roborevaturep1.model.RequestEntity;

/**
 * @author Dipanjali Ghosh
 *
 */
public class RequestDAO {

	private static Connection conn = null;
	private static Statement stmt = null;
	private static PreparedStatement pstmt = null;
	private static ResultSet rs = null;

	public static List<RequestEntity> readAll() {
		List<RequestEntity> users = new ArrayList<RequestEntity>();
		String query = "select * from public.request";
		connect();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				RequestEntity myRequest = new RequestEntity();
				myRequest.setRequest_id(rs.getInt("request_id"));
				myRequest.setEmployee_id(rs.getInt("employee_id"));
				myRequest.setAmount(rs.getBigDecimal("amount"));
				myRequest.setEmployee_reason(rs.getString("employee_reason"));
				myRequest.setTime_requested(rs.getTimestamp("time_requested"));
				myRequest.setManager_id(rs.getInt("manager_id"));
				myRequest.setIs_approved(rs.getBoolean("is_approved"));
				myRequest.setManager_reason(rs.getString("Manager_reason"));
				myRequest.setTime_reviewed(rs.getTimestamp("time_reviewed"));
				users.add(myRequest);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return users;
	}

	public static RequestEntity readById(int id) {
		RequestEntity myRequest = new RequestEntity();
		String query = "select * from public.request where role_number=" + id;
		connect();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
				myRequest.setRequest_id(rs.getInt("request_id"));
				myRequest.setEmployee_id(rs.getInt("employee_id"));
				myRequest.setAmount(rs.getBigDecimal("amount"));
				myRequest.setEmployee_reason(rs.getString("employee_reason"));
				myRequest.setTime_requested(rs.getTimestamp("time_requested"));
				myRequest.setManager_id(rs.getInt("manager_id"));
				myRequest.setIs_approved(rs.getBoolean("is_approved"));
				myRequest.setManager_reason(rs.getString("Manager_reason"));
				myRequest.setTime_reviewed(rs.getTimestamp("time_reviewed"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return myRequest;
	}

	/*
	 * For the save method, we're assuming that we only have the employee values
	 * */
	public static void save(RequestEntity newRequest) {
		String query = "insert into public.request (employee_id, amount, employee_reason, time_requested) values (?, ?, ?, ?);";
		connect();

		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, newRequest.getEmployee_id());
			pstmt.setBigDecimal(2, newRequest.getAmount());
			pstmt.setString(3, newRequest.getEmployee_reason());
			pstmt.setTimestamp(4, newRequest.getTime_requested());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}

	public static void delete(int id) {
		String query = "delete from public.request where request_id = " + id + ";";
		connect();
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}

	public static void update(int id, RequestEntity newRequest) {
		String query = "update public.request set employee_id=?, amount=?, employee_reason=?, time_requested=?, "
				+ "manager_id=?, is_approved=?, manager_reason=?, time_reviewed=? where request_id=?;";
		connect();
		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, newRequest.getEmployee_id());
			pstmt.setBigDecimal(2, newRequest.getAmount());
			pstmt.setString(3, newRequest.getEmployee_reason());
			pstmt.setTimestamp(4, newRequest.getTime_requested());
			pstmt.setInt(5, newRequest.getManager_id());
			pstmt.setBigDecimal(6, newRequest.getAmount());
			pstmt.setBoolean(7, newRequest.isIs_approved());
			pstmt.setString(8, newRequest.getManager_reason());
			pstmt.setTimestamp(9, newRequest.getTime_reviewed());
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		close();
	}

	public static Connection connect() {
		String dbUrl = "jdbc:postgresql://postgres.cuiwdhamwtzc.us-east-2.rds.amazonaws.com:5432/postgres";
		try {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(dbUrl, "postgres", "postgres");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void close() {
		try {
			if (conn != null) {
				conn.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (pstmt != null) {
				pstmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
