-- Table: public.staff_roles

DROP table if EXISTS public.staff_roles;

CREATE TABLE IF NOT EXISTS public.staff_roles
(
    role_number integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    role_name character varying(45) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT staff_roles_pkey PRIMARY KEY (role_number)
)

TABLESPACE pg_default;

ALTER TABLE public.staff_roles
    OWNER to postgres;

COMMENT ON TABLE public.staff_roles
    IS 'This table holds the roles that the end users could be.';
	

	

	
INSERT INTO public.staff_roles(
	role_name)
	VALUES ('Employee');
	
INSERT INTO public.staff_roles(
	role_name)
	VALUES ('Manager');

-- Table: public.users

DROP table if EXISTS public.users;

CREATE TABLE IF NOT EXISTS public.users
(
    user_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    username character varying(45) COLLATE pg_catalog."default" NOT NULL,
    password character varying(45) COLLATE pg_catalog."default" NOT NULL,
    real_name character varying(75) COLLATE pg_catalog."default" NOT NULL,
    email character varying(45) COLLATE pg_catalog."default",
    mobile bigint,
    role_number integer NOT NULL DEFAULT 1,
    CONSTRAINT user_id PRIMARY KEY (user_id),
    CONSTRAINT email UNIQUE (email),
    CONSTRAINT real_name UNIQUE (real_name),
    CONSTRAINT username UNIQUE (username),
    CONSTRAINT role_number FOREIGN KEY (role_number)
        REFERENCES public.staff_roles (role_number) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;

COMMENT ON TABLE public.users
    IS 'Holds the actual users in the company';

COMMENT ON COLUMN public.users.password
    IS 'Originally, I was planning on taking a moment to have the password hashed for security, but since it''s not required, I''m going to have it skipped for now.';

INSERT INTO public.users(
	username, password, real_name, email, mobile, role_number)
	VALUES ('Air', 'myPass', 'Anjali Ghosh', 'Air@gmail.com', 9496388778, 1);
	
INSERT INTO public.users(
	username, password, real_name, email, mobile, role_number)
	VALUES ('LibraKing', 'ChessRocks', 'Dipu Ghosh', 'libra@gmail.com', 9498199709, 2);
	
INSERT INTO public.users(
	username, password, real_name, email, mobile, role_number)
	VALUES ('axl171', 'myTempPass2', 'Joey Grayson', 'joey@gmail.com', 9495277379, 1);
	
INSERT INTO public.users(
	username, password, real_name, email, mobile, role_number)
	VALUES ('siva123', 'myPass3', 'Siva', 'siva@gmail.com', 9941454984, 2);

-- Table: public.request

DROP TABLE if EXISTS public.request;

CREATE TABLE IF NOT EXISTS public.request
(
    request_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    employee_id integer NOT NULL,
    amount numeric(13,2),
    employee_reason text COLLATE pg_catalog."default",
    time_requested timestamp without time zone NOT NULL,
    manager_id integer,
    is_approved boolean,
    manager_reason text COLLATE pg_catalog."default",
    time_reviewed timestamp without time zone,
    CONSTRAINT request_id PRIMARY KEY (request_id),
    CONSTRAINT employee_id FOREIGN KEY (employee_id)
        REFERENCES public.users (user_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE public.request
    OWNER to postgres;

COMMENT ON TABLE public.request
    IS 'This table will house the requests for reimbursement that Employees file and Managers review.';

INSERT INTO public.request(
	employee_id, amount, employee_reason, time_requested)
	VALUES (1, 20.99, 'equipment cost', '2021-10-20 12:07:19.954253');
	
UPDATE public.request
	SET manager_id=2, is_approved= true , manager_reason='equipment was used', time_reviewed='2021-10-20 12:11:05.982458'
	WHERE request_id=1;
	
INSERT INTO public.request(
	employee_id, amount, employee_reason, time_requested)
	VALUES (3, 30.00, 'video games', '2021-11-20 12:07:19.954253');
	
UPDATE public.request
	SET manager_id=2, is_approved= false , manager_reason='Video games are not part of the work that needs to be done', time_reviewed= now()
	WHERE request_id=2;